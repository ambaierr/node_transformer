import argparse
import sys
import subprocess

ap = argparse.ArgumentParser()
ap.add_argument('--array_index', nargs=1, type=int, required=True)
opts = ap.parse_args()
array_index = opts.array_index[0]       # Input --array_index is expected to be in {0, 1, ..., 31}

# Other settings
node = 1
time_dependent_mha = 0
mha_skip = 0
max_string_length = 10

d_model = 8
n_layers = 2

n_epochs = 1000000
n_seconds = int(19.9*60)

# Loop to correct run
count = 0
for repeats in range(0, 12):
	for lr in [1e-2, 8e-3, 6e-3, 4e-3, 2e-3, 1e-3]:
		for reg_lam_index in range(0, 16):

			if reg_lam_index == 0:
				reg_lam = 0.0
			else:
				reg_lam = 4.0 ** (2 - reg_lam_index)	# Ranges from 4.0 to 1.5e-8

			if (count % 32) == array_index:
				subprocess.call(["python3", "train.py",
					"--d_model", str(d_model),
					"--n_layers", str(n_layers),

					"--node", str(node),
					"--time_dependent_mha", str(time_dependent_mha),
					"--mha_skip", str(mha_skip),
					"--reg_lam", str(reg_lam),

					"--max_string_length", str(max_string_length),
					"--lr", str(lr),

					"--n_epochs", str(n_epochs),
					"--n_seconds",  str(n_seconds),
					"--save", "out/%d" % count])

			count += 1
