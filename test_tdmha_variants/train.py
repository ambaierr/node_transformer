import os
import argparse
import pickle
import time
import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torch.autograd import Variable
import data
import model_parts

# Parse arguments
parser = argparse.ArgumentParser()
parser.add_argument('--d_model', type=int, required=True)
parser.add_argument('--n_layers', type=int, required=True)

parser.add_argument('--node', type=int, default=0, required=False)
parser.add_argument('--time_dependent_mha', type=int, default=0, required=False)
parser.add_argument('--mha_skip', type=int, default=0, required=False)

parser.add_argument('--max_string_length', type=int, required=True)
parser.add_argument('--lr', type=float, required=True)

parser.add_argument('--n_epochs', type=int, required=True)
parser.add_argument('--n_seconds', type=int, required=True)
parser.add_argument('--save', type=str, required=True)
args = parser.parse_args()

print(args, flush=True)

# Initialize torch
torch.set_num_threads(1)
device = torch.device('cpu')

# General model configuration
d_model = args.d_model
nhead = d_model // 2
dim_feedforward = d_model
num_transformer_layers = args.n_layers
sos = True

# NODE transformer configuration
node_transformer = bool(args.node)
time_dependent_mha = bool(args.time_dependent_mha)
mha_skip = bool(args.mha_skip)
t_end = 1.0
method = "dopri5"
atol = rtol = 1e-5

# Build model
transformer_blocks = []
for k in range(0, num_transformer_layers):
	if node_transformer:
		odefunc = model_parts.NODETransformerEncoderFunc(d_model, nhead, dim_feedforward, time_dependent_mha, mha_skip)
		transformer_blocks += [model_parts.ODEBlock(odefunc, t_end, method, atol, rtol)]
	else:
		transformer_blocks += [model_parts.VanillaTransformerBlock(d_model, nhead, dim_feedforward)]

def apply_mask(mask_to_apply):
	for block in transformer_blocks:
		block.apply_mask(mask_to_apply)

layers = [model_parts.BinaryEmbedding(d_model)] + transformer_blocks + [model_parts.Prediction(d_model, sos)]
model = nn.Sequential(*layers)

n_params = sum(p.numel() for p in model.parameters() if p.requires_grad)
print("Number of parameters: %d" % n_params, flush=True)

# Training dataset
max_string_length = args.max_string_length
n_points = 2 * ((2**max_string_length) - 1)
features, labels, mask = data.masked_strings_of_length_leq_n(max_string_length, not sos)
features = torch.from_numpy(features).float()
labels = torch.from_numpy(labels).long()
mask = torch.from_numpy(mask).long()

# Loss and optimizer
criterion = nn.CrossEntropyLoss()
optimizer = torch.optim.Adam(model.parameters(), lr=args.lr)
batch_size = n_points

# Train epochs
n_epochs = args.n_epochs
n_seconds = args.n_seconds

training_acc_list = np.zeros(n_epochs)
times_list = np.zeros(n_epochs)
percent_zeros_list = np.zeros(n_epochs)
last_index = -1

time_start = time.time()
for epoch in range(0, n_epochs):

	try:

		# Train through the mini-batches
		permutation = torch.randperm(n_points)

		for i in range(0, n_points, batch_size):
			indices = permutation[i : i + batch_size]
			batch_x = Variable(features[indices, :, :])
			batch_y = Variable(labels[indices])
			apply_mask(mask[indices, :, :])

			optimizer.zero_grad()
			outputs = model(batch_x)
			loss = criterion(outputs, batch_y)
			loss.backward()
			optimizer.step()

		# Evaluate training and validation accuracy
		with torch.no_grad():
			training_x = Variable(features)
			training_y = Variable(labels)
			apply_mask(mask)
			training_out = model(training_x)
			loss = criterion(training_out, training_y)
			training_out = torch.argmax(training_out, axis=1)
			training_acc = (torch.sum(training_out == labels)).numpy() / n_points
			percent_zeros = (torch.sum(training_out == torch.zeros_like(training_out))).numpy() / n_points

	except Exception as e:
		print("Error during training/evaluation:", flush=True)
		print(e, flush=True)
		break

	# Update information
	time_now = time.time()
	print("Epoch: %d, Seconds Elapsed: %f, Training Loss: %e, Training Acc: %f, Percent Class 0 Predictions: %f" \
		% (epoch, time_now - time_start, loss, training_acc, percent_zeros), flush=True)

	training_acc_list[epoch] = training_acc
	times_list[epoch] = time_now - time_start
	percent_zeros_list[epoch] = percent_zeros
	last_index = epoch

	# Decide if finished
	if time_now - time_start >= n_seconds:
		print("Maximum time elapsed - exiting.", flush=True)
		break

# Save information
data_save = {"args" : args, "training_acc_list" : training_acc_list, "times_list" : times_list, \
	"percent_zeros_list" : percent_zeros_list, "last_index" : last_index, "n_params" : n_params}

with open(args.save + ".pickle", "wb") as handle:
	pickle.dump(data_save, handle, protocol=pickle.HIGHEST_PROTOCOL)
