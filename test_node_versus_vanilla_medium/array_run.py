import argparse
import sys
import subprocess

ap = argparse.ArgumentParser()
ap.add_argument('--array_index', nargs=1, type=int, required=True)
opts = ap.parse_args()
array_index = opts.array_index[0]       # Input --array_index is expected to be in {0, 1, ..., 63}

# Other settings
time_dependent_mha = 0
mha_skip = 0

max_string_length = 8
n_epochs = 100000
n_seconds = int(19.9*60)

# Loop to correct run
count = 0
for repeats in range(0, 12):
	for lr in [1e-2, 8e-3, 6e-3, 4e-3, 2e-3, 1e-3]:
		for d_model in [4, 6, 8, 10]:
			for n_layers in [1, 2, 3, 4]:
				for node in [0, 1]:
					if (count % 64) == array_index:
						subprocess.call(["python3", "train.py",
							"--d_model", str(d_model),
							"--n_layers", str(n_layers),

							"--node", str(node),
							"--time_dependent_mha", str(time_dependent_mha),
							"--mha_skip", str(mha_skip),

							"--max_string_length", str(max_string_length),
							"--lr", str(lr),

							"--n_epochs", str(n_epochs),
							"--n_seconds",  str(n_seconds),
							"--save", "out/%d" % count])

					count += 1
