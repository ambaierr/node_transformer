PyTorch implementation of the experiments from our N-ODE Transformer paper (https://arxiv.org/abs/2010.11358).

To run our code, you must install torchdiffeq (https://github.com/rtqichen/torchdiffeq).
