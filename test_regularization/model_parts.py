import numpy as np
import torch
import torch.nn as nn
import torch.nn.functional as F
from torchdiffeq import odeint as odeint

class ConcatLinear(nn.Module):
	def __init__(self, dim_in, dim_out):
		super(ConcatLinear, self).__init__()
		self._layer = nn.Linear(dim_in + 1, dim_out)

	def forward(self, t, x):
		tt = torch.ones_like(x[:, :, :1]) * t
		ttx = torch.cat([tt, x], 2)
		return self._layer(ttx)

class ScaledDotProductAttention(nn.Module):
	def forward(self, query, key, value, mask=None):
		dk = query.size()[-1]
		scores = query.matmul(key.transpose(-2, -1)) / np.sqrt(dk)
		if mask is not None:
			scores = scores.masked_fill(mask.transpose(-2, -1) == 0, -1e9)
		attention = F.softmax(scores, dim=-1)
		return attention.matmul(value)

class TimeDependentMultiHeadAttention(nn.Module):

	def __init__(self, d_model, head_num, time_dependent):
		super(TimeDependentMultiHeadAttention, self).__init__()

		if d_model % head_num != 0:
			raise ValueError('`d_model`({}) should be divisible by `head_num`({})'.format(d_model, head_num))

		self.d_model = d_model
		self.head_num = head_num
		self.time_dependent = time_dependent

		if self.time_dependent:
			self.linear_q = ConcatLinear(d_model, d_model)
			self.linear_k = ConcatLinear(d_model, d_model)
			self.linear_v = ConcatLinear(d_model, d_model)
			self.linear_o = ConcatLinear(d_model, d_model)
		else:
			self.linear_q = nn.Linear(d_model, d_model)
			self.linear_k = nn.Linear(d_model, d_model)
			self.linear_v = nn.Linear(d_model, d_model)
			self.linear_o = nn.Linear(d_model, d_model)

		self.sdpa = ScaledDotProductAttention()

	def forward(self, t, q, k, v, mask=None):
		if self.time_dependent:
			  q, k, v = self.linear_q(t, q), self.linear_k(t, k), self.linear_v(t, v)
		else:
			  q, k, v = self.linear_q(q), self.linear_k(k), self.linear_v(v)

		q = self._reshape_to_batches(q)
		k = self._reshape_to_batches(k)
		v = self._reshape_to_batches(v)

		if mask is not None:
			batch_size, seq_len, one = mask.size()
			mask = mask.reshape(batch_size, seq_len, 1, 1)\
					   .repeat(1, 1, self.head_num, 1)\
					   .permute(0, 2, 1, 3)

		y = self.sdpa(q, k, v, mask)
		y = self._reshape_from_batches(y)

		if self.time_dependent:
			  y = self.linear_o(t, y)
		else:
			  y = self.linear_o(y)

		return y

	def _reshape_to_batches(self, x):
		batch_size, seq_len, d_model = x.size()
		sub_dim = self.d_model // self.head_num
		return x.reshape(batch_size, seq_len, self.head_num, sub_dim)\
				.permute(0, 2, 1, 3)

	def _reshape_from_batches(self, x):
		batch_size, head_num, seq_len, sub_dim = x.size()
		out_dim = self.d_model
		return x.permute(0, 2, 1, 3)\
				.reshape(batch_size, seq_len, out_dim)

class VanillaTransformerBlock(nn.Module):
	def __init__(self, d_model, nhead, dim_feedforward, activation=F.relu):
		super(VanillaTransformerBlock, self).__init__()
		self.self_attn = TimeDependentMultiHeadAttention(d_model, nhead, False)
		self.linear1 = nn.Linear(d_model, dim_feedforward)
		self.linear2 = nn.Linear(dim_feedforward, d_model)
		self.activation = activation
		self.mask = None

	def __setstate__(self, state):
		if 'activation' not in state:
			state['activation'] = F.relu
		super(VanillaTransformerBlock, self).__setstate__(state)

	def forward(self, src):
		src2 = self.self_attn(None, src, src, src, self.mask)
		src2 += src

		src3 = self.linear2(self.activation(self.linear1(src2)))
		src3 += src2

		return src3

	def apply_mask(self, mask):
		self.mask = mask

class NODETransformerEncoderFunc(nn.Module):
	def __init__(self, d_model, nhead, dim_feedforward, time_dependent_mha, mha_skip, activation=F.elu):
		super(NODETransformerEncoderFunc, self).__init__()
		self.mha_skip = mha_skip
		self.self_attn = TimeDependentMultiHeadAttention(d_model, nhead, time_dependent_mha)
		self.linear1 = ConcatLinear(d_model, dim_feedforward)
		self.linear2 = ConcatLinear(dim_feedforward, d_model)
		self.activation = activation
		self.mask = None

	def __setstate__(self, state):
		if 'activation' not in state:
			state['activation'] = F.elu
		super(NODETransformerEncoderFunc, self).__setstate__(state)

	def forward(self, t, src_reg):

		src = src_reg[:, :, :-1]

		src2 = self.self_attn(t, src, src, src, self.mask)
		if self.mha_skip:
			src2 += src

		src3 = self.linear2(t, self.activation(self.linear1(t, src2)))

		if self.mask is not None:
			src3 = src3.masked_fill(self.mask == 0, 0.0)

		src3_norm = 0.5 * torch.norm(src3, p=2)
		output = torch.empty_like(src_reg)
		output[:, :, :-1] = src3
		output[:, :, -1] = src3_norm
		return output

class ODEBlock(nn.Module):
	def __init__(self, odefunc, t_end, method, atol, rtol):
		super(ODEBlock, self).__init__()
		self.odefunc = odefunc
		self.integration_time = torch.tensor([0, t_end]).float()
		self.method = method
		self.atol = atol
		self.rtol = rtol
		self.reg = None

	def forward(self, x):	# x is (batch x seq_l x d_model)
		self.integration_time = self.integration_time.type_as(x)
		x = F.pad(x, (0, 1), "constant", 0)		# Pad to keep track of rhs norm. x is now x is (batch x seq_l x (d_model + 1)).
		out = odeint(self.odefunc, x, self.integration_time, method=self.method, atol=self.atol, rtol=self.rtol)
		self.reg = (out[-1])[:, :, -1]
		return (out[-1])[:, :, :-1]

	def apply_mask(self, mask):
		self.odefunc.mask = mask

class BinaryEmbedding(nn.Module):
	def __init__(self, d_model):
		super(BinaryEmbedding, self).__init__()
		self.zero_embedding = nn.Linear(1, d_model, bias=False)
		self.one_embedding = nn.Linear(1, d_model, bias=False)
		self.sos_embedding = nn.Linear(1, d_model, bias=False)

	def forward(self, x):
		return self.zero_embedding(x[:, :, 0:1]) + self.one_embedding(x[:, :, 1:2])\
			+ self.sos_embedding(x[:, :, 2:3])

class Prediction(nn.Module):
	def __init__(self, d_model, sos, activation=F.relu):
		super(Prediction, self).__init__()
		self.linear1 = nn.Linear(d_model, d_model)
		self.linear2 = nn.Linear(d_model, 2)
		self.sos = sos
		self.activation = activation

	def forward(self, x):
		if self.sos:
			out = self.linear2(self.activation(self.linear1(x[:, 0, :])))
		else:
			out = self.linear2(self.activation(self.linear1(x[:, 1, :])))
		return out
