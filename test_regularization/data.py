import numpy as np

def strings_of_length_n(n):
	n_points = 2**n

	features = np.empty((n_points, n + 1, 3))       # [1, 0, 0] means 0 bit. [0, 1, 0] means 1 bit. [0, 0, 1] means SOS.
	labels = np.empty((n_points))                   # 0 means odd number of 1 bits. 1 means even number of 1 bits.

	for k in range(0, n_points):
		features[k, 0, :] = [0, 0, 1]

		even_num_one_bits = True
		k_ = k
		for l in range(1, n + 1):
			is_bit_a_one = bool(k_ % 2)
			k_ //= 2
			if is_bit_a_one:
				even_num_one_bits = not even_num_one_bits
				features[k, l, :] = [0, 1, 0]
			else:
				features[k, l, :] = [1, 0, 0]
		labels[k] = int(even_num_one_bits)

	return features, labels

def masked_strings_of_length_leq_n(n, mask_sos):

	n_tot = 2*(2**n - 1)

	features = np.zeros((n_tot, n + 1, 3))
	labels = np.zeros((n_tot))
	mask = np.zeros((n_tot, n + 1, 1))

	index_start = 0
	for l in range(1, n + 1):

		features_l, labels_l = strings_of_length_n(l)

		features[index_start:(index_start + 2**l), 0:(l+1), :] = features_l
		labels[index_start:(index_start + 2**l)] = labels_l
		mask[index_start:(index_start + 2**l), 0:(l+1), :] = np.ones((2**l, l+1, 1))

		index_start += 2**l

	if mask_sos:
		mask[:, 0, 0] = np.zeros(n_tot)

	return features, labels, mask
